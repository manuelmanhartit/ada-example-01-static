![picture](ada/logo.png)

# Ada Example 01 - static pages - Version 1.2

This is the first and easiest example of a static website built with ADA (Alpha Dust Advanced).

So it is merely a showcase project to show how easy static websites can be created with ADA framework.

## Getting started (Installing / Running)

To start this, place it into a php supporting webserver (Apache, Nginx,...)

You can also use a php docker container, actually it was developed with a fork of [Dockerized PHP](https://github.com/mikechernev/dockerised-php.git).

## Creation of static html content pages

Here I explain what I did to create this project - you can take the project and adapt from here or start from green field:

1. Create project directory
2. Initialize git

	git init

3. Checkout ADA as git submodule

	git submodule add git@bitbucket.org:manuelmanhartit/ada.git

4. Create an `index.php` with following content

	<?php
	// include the Ada framework
	require_once('ada/framework.php');
	// actually run it
	$ada = new Ada();
	// put current year into the context for the copyright
	$currentYear = date('Y');
	$ada->addContextValue("currentYear", $currentYear);
	// print the template and the body html file
	$ada->printHtmlPage();
	?>

5. Create a workspace directory with at least the following structure / files

	/ada-files        ... the files used for templating (default workspace dir)
		/ template.html    ... the template file in which the content files will be rendered in
		/ site-config.json ... the configuration file for the site (will be used in template.html heavily and available in all templates)
		/ content          ... all html templates (rendered in the content body section)

6. Adapt `ada-files/template.html`, `ada-files/site-config.json`, `ada-files/content/*.html`, etc. to your needs. \

Wow, you have created a website, and it was really fast too.

If you do not like the default paths, you can easily change them via the configuration.

## Prerequisites

* Any modern browser
* PHP development environment (if using docker eg. https://github.com/mikechernev/dockerised-php.git)

## Installing / Running

(optional)
If you use Sass for creating your css and want auto compiling the scss run

    # sass main.scss main.css --watch

Place the project directory in your webserver / php dev environment and call it (eg. http://127.0.0.1/).

## Built With

* [PHP 7.3.0](http://www.php.net/)
* [Twig 2.6.0](https://twig.symfony.com/)

## Contributing

No contributing planned since this is a showcase / example project. But you are invited to clone it and build your own static website based upon it.

## Versioning

This project uses a major and a minor version as versioning system. You can find it on top of this file.

## Authors

* **Manuel Manhart** - *Initial work*

## License

This project is licensed under MIT License - see the [LICENSE](https://opensource.org/licenses/MIT) for details.
